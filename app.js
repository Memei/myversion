const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://RMS:Admin123@zuitt-batch197.kblestk.mongodb.net/practice?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection
db.on("error", console.error.bind(console, "Connection Error"))
db.once("open",()=>console.log("Connected To MongoDB"))

app.use(express.json())

const userRoutes = require('./routes/userRoutes')
app.use('/users',userRoutes)


app.listen(port, ()=>console.log(`Server running at port ${port}`))
