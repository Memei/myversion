const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.registerUser = (req,res) => {
	console.log(req.body);
	if(req.body.password.length < 8) return res.send({message: "Password is too short."})
	const hashedPW = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPW);
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	})
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err))
};

module.exports.loginUser = (req,res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send({message: "No User Found."})
		} else {
		const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password);
		console.log(isPasswordCorrect);
			if(isPasswordCorrect){
				return res.send({accessToken: createAccessToken(result)});
			} else {
				return res.send({message: "Password is incorrect."})
			}
		}
	})
	.catch(err => res.send(err))
};

module.exports.getSingleUser = (req,res) => {
	console.log(req.body);
	User.findById(req.body.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

module.exports.checkEmailExists = (req,res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send({
				isAvailable: true,
				message: "Email Available."
			})
		} else {
			return res.send({
				isAvailable: false,
				message: "Email is already registered."
			})
		}
	})
	.catch(err => res.send(err));

};
