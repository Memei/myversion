const express = require('express')
const router = express.Router()
const userControllers = require('../controllers/userControllers')

const {
	registerUser,
	loginUser,
	getSingleUser,
	checkEmailExists,
} = userControllers

router.post('/register',registerUser);

router.post('/login',loginUser);

router.post('/details', getSingleUser);

router.post('/checkEmailExists',checkEmailExists)

module.exports = router;